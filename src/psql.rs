use postgres;
use postgres::Connection;
use postgres::TlsMode;
use PG_DB;
use PG_PASSWD;
use PG_USER;
use PG_HOST;



const NONE: &[&postgres::types::ToSql] = &[];









pub fn check_tables() {
	let conn = connect();

	println!("{:#?}", conn);


	let res = conn.query("SELECT * FROM foo;", NONE);


	println!("{:#?}", res);



}







pub fn init_tables(user: String, passwd: String, db: String) {

	let conn = connect();

	conn.execute("DROP TABLE names, infos, ratings", &[]);
}





pub fn clean_database(user: String, passwd: String, db: String) {

	let conn = connect();

	conn.execute("DROP TABLE names, infos, ratings", &[]);
}



pub fn connect() -> postgres::Connection {
	let conn_str = format!("postgres://{}:{}@{}/{}", PG_USER, PG_PASSWD, PG_HOST, PG_DB);

	Connection::connect(conn_str, TlsMode::None).unwrap()
}
