use std::fmt;
use chrono;



#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum RessourceLocation {
	InfoComments,
	UserList,
	Info,
	User,
	GetTopten,
}








#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Task {
	InfoUpdate {
		info_id: i64
	}
}


impl fmt::Display for Task  {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
		match *self {
			Task::InfoUpdate {info_id} => write!(fmt, "InfoUpdate")
		}
	}
}



#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Job {
	#[serde(rename = "id")]
	pub id: isize,
	pub priority: i64,
	pub added: chrono::DateTime<chrono::Utc>,
	pub status: super::OrderStatus,
	pub task: Task,
}










impl Job {
	pub fn new(task: Task) -> Self {
		Self {
			id: 0,
			priority: 0,
			added: chrono::Utc::now(),
			status: super::OrderStatus::waiting(),
			task: task
		}
	}


	fn set_status(&mut self, new_status: super::OrderStatus) -> Self {
		self.status = new_status;
		self.clone()
	}
}
