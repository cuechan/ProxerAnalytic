use mongodb;
use mongodb::coll;
use mongodb::db::Database;
use mongodb::db::ThreadedDatabase;
use mongodb::ThreadedClient;
use proxer::response::*;
use bson;
use docs;
use docs::Docs;
use serde::Serialize;
use chrono;
use docs::ToDoc;




const DB_HOST: &str = "localhost";
const DB_DBNAME: &str = "ProxerAnalytic";
const UPDATE_UPSERT: coll::options::UpdateOptions = coll::options::UpdateOptions {
	upsert: Some(true),
	write_concern: None
};







pub trait Collectable<T> {
	fn collect(&self, T);
}





impl Collectable<info::Fullentry> for Collector {
	fn collect(&self, info: info::Fullentry) {
		unimplemented!()
	}
}


impl Collectable<info::Comment> for Collector {
	fn collect(&self, info: info::Comment) {
		unimplemented!()
	}
}












#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Context {
	User {uid: i64},
	Info {id: i64}
}




#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TimedDocument<T> {
	// context: Vec<Context>,
	datetime: bson::UtcDateTime,
	data: T
}


pub trait Storable: Serialize {
	const COLLECTION: &'static str;

	fn filter(&self) -> bson::Document;
}







/// this service can be feeded with data that is then placed in the database
pub struct Collector {
	db: Database,
}



impl Collector {
	pub fn new() -> Self {
		let mdb = mongodb::Client::connect(DB_HOST, 27017)
			.unwrap()
			.db(DB_DBNAME);

		Self {
			db: mdb
		}
	}




	pub fn process<T: Storable>(&self, c: T) {
		let coll = self.coll(T::COLLECTION);


		// check if document has changed

		let res = coll.find (
			Some(c.filter()),
			None
		).unwrap();


		if res.count() >= 0 {

			let new = TimedDocument {
				datetime: bson::UtcDateTime::from(chrono::Utc::now()),
				data: c
			};


			coll.insert_one(
				bson::to_bson(&new).unwrap().as_document().unwrap().to_owned(),
				None
			);
		}
		else {
			warn!("document already there");
		}
	}




	pub fn process_info(&self, info: docs::Info) {
		let coll = self.coll("Info");



		let res = coll.find (
			Some(info.filter()),
			None
		).unwrap();


		if res.count() >= 0 {

			let new = TimedDocument {
				datetime: bson::UtcDateTime::from(chrono::Utc::now()),
				data: info
			};


			coll.insert_one(
				bson::to_bson(&new).unwrap().as_document().unwrap().to_owned(),
				None
			);
		}
	}




	pub fn collect_info_info(&self, c: docs::Info) -> Option<()> {
		let coll = self.coll("AllInfos");


		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();



		None
	}


	pub fn collect_comment(&self, c: docs::Comment) -> Option<()> {
		let coll = self.coll("AllComments");


		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();



		None
	}


	pub fn collect_rating(&self, c: docs::Rating) -> Option<()> {
		let coll = self.coll("AllRatings");


		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();

		None
	}


	pub fn collect_progress(&self, c: docs::Progress) -> Option<()> {
		let coll = self.coll("AllProgress");


		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();

		None
	}





	pub fn collect_data(&self, data: docs::Docs) -> Option<()> {

		debug!("adding data for {}", data);

		match data {
			Docs::Comment(c) => self.add_comment(c),
			Docs::Genre(c) => self.add_genre(c),
			Docs::Rating(c) => self.add_rating(c),
			Docs::Userinfo(c) => self.add_userinfo(c),
			Docs::Progress(c) => self.add_progress(c),
			_ => error!("datatype not supported yet"),
		}

		Some(())
	}














	fn add_comment(&self, c: docs::Comment) {
		let coll = self.coll("Comments");


		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();
	}


	fn add_genre(&self, c: docs::Genre) {
		let coll = self.coll("Genres");

		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();
	}


	fn add_rating(&self, c: docs::Rating) {
		let coll = self.coll("Ratings");

		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();
	}


	fn add_progress(&self, c: docs::Progress) {
		let coll = self.coll("Progress");

		println!("{:#?}", c);


		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();
	}


	fn add_userinfo(&self, c: docs::Userinfo) {
		let coll = self.coll("Userinfo");


		let time = c.status_time;

		let timestamp = time.timestamp();


		coll.update_one(
			c.to_doc(),
			doc! {"$set": c.to_doc()},
			Some(UPDATE_UPSERT)
		).unwrap();
	}


	fn coll(&self, coll: &str) -> coll::Collection {
		self.db.collection(coll)
	}
}
