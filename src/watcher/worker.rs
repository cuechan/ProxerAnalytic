use proxer;
use mongodb;
use std::time;
use watcher::collector;
use docs;
use watcher::queue;
use watcher::queue::QueuedObjectType;
use watcher::squeezer;








pub struct Worker {
	proxer: proxer::Client,
	collector: collector::Collector,
}



impl Worker {
	pub fn new() -> Self {
		let mut client = proxer::Client::with_env_key("PROXER_API_KEY").unwrap();
		client.cooldown_time(time::Duration::new(3, 500_000_000));

		Self {
			proxer: client,
			collector: collector::Collector::new(),
		}
	}




	pub fn process_job(&mut self, job: queue::QueuedObject) -> Option<()> {
		match job.type_ {
			QueuedObjectType::Info {id} => self.job_info(id),
			QueuedObjectType::InfoComments {id} => self.job_infocomments(id),
			QueuedObjectType::User {uid} => {
				let mut squeezed = Vec::new();


				debug!("job for User {}", uid);


				let profile = self.proxer.execute(
					proxer::api::user::Userinfo::uid(uid)
				).unwrap();

				squeezed.append(&mut squeezer::squeeze_from_userinfo(profile));



				squeezed.into_iter().for_each(|x| {
					self.collector.collect_data(x);
				});
			}

			_ => error!("can't process job"),
		}

		None

	}



	pub fn job_info(&mut self, id: i64) {
		let info = self.proxer.execute(
			proxer::api::info::GetFullEntry::with_default(id as usize)
		).unwrap();

		let info = docs::Info::from(info);


		self.collector.collect_info_info(info);
	}




	pub fn job_infocomments(&self, id: i64) {
		debug!("Downloading Comments...");

		let pager = self.proxer.clone().pager(
			proxer::api::info::GetComments {
				id: id as usize,
				limit: None,
				p: None,
				sort: None,
			}
		);


		pager.for_each(|x| {
			let comment = x.unwrap();


			self.collector.collect_comment(docs::Comment::from(comment.clone()));
			self.collector.collect_rating(docs::Rating::from(comment.clone()));
			self.collector.collect_progress(docs::Progress::from(comment.clone()));


			// info!("adding user to queue: {}", comment.username);
			// let new = queue::QueuedObject {
			// 	type_: QueuedObjectType::User { uid: comment.uid },
			// 	score: 10,
			// };
		});
	}
}
