use proxer;
use std::*;




pub fn download_info(id: i64) -> Result<proxer::response::info::Fullentry, ()> {

	let request = proxer::api::info::GetFullEntry { id: id as usize };



	let mut prxr = proxer::Client::with_env_key("PROXER_API_KEY").unwrap();

	debug!("↔️ connecting to api");
	let response = prxr.execute(request);
	debug!("↔️ data received");


	match response {
		Ok(i) => println!("{:#?}", i),
		Err(e) => eprintln!("{:#?}", e)
	}

	Err(())
}
