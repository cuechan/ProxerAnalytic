pub mod dl;
pub mod scheduler;

use std::*;
use std::thread;
use watcher::job;





const COLLECTION: &str = "Jobs";



/// take information about missing or changed data,
/// basically whenever something has changed or is needed to update,
/// the watcher finds the most efficient way to get this information
/// and schedules how this is happening






/// actually just routing the task to its corresponding function/module
fn exec_task(task: job::Task) -> Result<time::Duration, time::Duration> {
	use watcher::job::Task;

	let started = time::Instant::now();

	println!("{:#?}", task);

	match task {
		Task::InfoUpdate {info_id} => info_update(info_id).unwrap(),
		_ => error!("❌ job type is not supported currently"),

	}

	Ok(started.elapsed())
}







pub struct TaskResult {
	duration: time::Duration,
}


impl TaskResult {
	fn new(dur: time::Duration) -> Self {
		Self {
			duration: dur
		}
	}


	fn duration_secs(&self) -> u64 {
		self.duration.as_secs()
	}
}





fn info_update(id: i64) -> Result<(), ()> {
	debug!("updating {:?}", id);

	dl::download_info(45645654);

	Err(())
}
