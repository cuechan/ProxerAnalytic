use mongodb::db::Database;
use mongodb::db::ThreadedDatabase;
use mongodb::ThreadedClient;
use mongodb;
use bson;
use mongodb::coll;
use super::DB_HOST;
use super::DB_DBNAME;
use super::AsBsonDoc;





pub struct Queue {
	coll: coll::Collection,
}


impl Queue {
	pub fn new() -> Self {
		let coll = mongodb::Client::connect(DB_HOST, 27017)
			.unwrap()
			.db(DB_DBNAME)
			.collection("Queue");


		Self {
			coll: coll
		}
	}


	pub fn with_dbclient(client: mongodb::Client) -> Self {
		let coll = client.db(DB_DBNAME).collection("Queue");

		Self {
			coll: coll
		}
	}
}




impl Iterator for Queue {
	type Item = QueuedObject;

	fn next(&mut self) -> Option<Self::Item> {
		let mut find = coll::options::FindOneAndDeleteOptions::new();

		find.sort = Some(doc!{"score": -1});

		let job = self.coll.find_one_and_delete(doc!{}, Some(find)).unwrap();


		match job {
			None => None,
			Some(r) => Some(bson::from_bson(bson::Bson::Document(r)).unwrap())
		}
	}
}



#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "jobtype", content = "jobdata")]
pub enum QueuedObjectType {
	Info {id: i64},
	InfoComments {id: i64},
	User {uid: i64},
	UserComments {uid: i64},
	UserTopTen {uid: i64},
}

impl AsBsonDoc for QueuedObject {}





/// represents an object in the queue
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct QueuedObject {
	#[serde(rename = "type")]
	pub type_: QueuedObjectType,
	pub score: i64,
}
