//! Squeezes the usefull data out from api responses

use proxer::response::*;
use docs;



pub fn squeeze_from_fullentry(data: info::Fullentry) -> Vec<docs::Docs> {
	let mut squeezed: Vec<docs::Docs> = Vec::new();


	squeezed.push(docs::Docs::Info(docs::Info::from(data.clone())));

	data.tags.into_iter().for_each(|x| {
		let tag = docs::Tag::from(x);
		squeezed.push(docs::Docs::Tag(tag));
	});

	data.genre.into_iter().for_each(|x| {
		let genre = docs::Genre::from(x);
		squeezed.push(docs::Docs::Genre(genre));
	});

	squeezed
}


pub fn squeeze_from_comment(data: info::Comment) -> Vec<docs::Docs> {
	let mut squeezed: Vec<docs::Docs> = Vec::new();

	squeezed.push(docs::Comment::from(data.clone()).into());

	squeezed.push(docs::Rating::from(data.clone()).into());

	squeezed.push(docs::Progress::from(data.clone()).into());

	squeezed
}


pub fn squeeze_from_userinfo(data: user::Userinfo) -> Vec<docs::Docs> {
	let mut squeezed: Vec<docs::Docs> = Vec::new();

	squeezed.push(docs::Userinfo::from(data).into());

	squeezed
}
