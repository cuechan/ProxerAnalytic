pub mod service;
pub mod job;
pub mod collection;
pub mod squeezer;
pub mod collector;
pub mod queue;
pub mod worker;

use bson;
use chrono;
use chrono::DateTime;
use chrono::FixedOffset;
use watcher::collector::Collectable;
use crypto::digest::Digest;
use crypto::sha2::Sha256;
use docs;
use docs::Docs;
use docs::ToDoc;
use mongodb;
use mongodb::coll;
use mongodb::coll::Collection;
use mongodb::db::Database;
use mongodb::db::ThreadedDatabase;
use mongodb::ThreadedClient;
use proxer;
use proxer::PageableEndpoint;
use proxer::response as pres;
use serde;
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::*;
use std::collections::hash_map::DefaultHasher;
use std::hash;
use std::hash::Hash;
use std::hash::Hasher;
use std::ops::Add;
use std::time;
use watcher::queue::Queue;
use watcher::queue::QueuedObject;
use watcher::queue::QueuedObjectType;


type Timestamp = DateTime<FixedOffset>;



pub const DB_HOST: &str = "localhost";
pub const DB_DBNAME: &str = "ProxerAnalytic";
const UPDATE_UPSERT: coll::options::UpdateOptions = coll::options::UpdateOptions {
	upsert: Some(true),
	write_concern: None
};


pub fn start()
{
	let mut p_watcher = ProxerWatcher::new();



	loop {
		p_watcher.step_fwd();
	}
}




/// take information about missing or changed data,
/// basically whenever something has changed or is needed to update,
/// the watcher finds the most efficient way to get this information
/// and schedules how this is happening











// maybe moved to job.rs

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "status", rename_all = "lowercase")]
pub enum OrderStatus {
	Successful {
		time: chrono::DateTime<chrono::Utc>,
	},
	Failed {
		time: chrono::DateTime<chrono::Utc>
	},
	Waiting {
		time: chrono::DateTime<chrono::Utc>,
	},
}


impl OrderStatus {
	#[allow(dead_code)]
	fn now_succeeded() -> Self {
		OrderStatus::Successful {
			time: chrono::Utc::now()
		}
	}


	fn now_failed() -> Self {
		OrderStatus::Failed {
			time: chrono::Utc::now()
		}
	}


	fn waiting() -> Self {
		OrderStatus::Waiting {
			time: chrono::Utc::now()
		}
	}
}





#[derive(Clone)]
pub struct OrderHandle {
	job: job::Job,
}



pub struct WatcherConfig {

}




pub struct ProxerWatcher {
	db: Database,
	config: WatcherConfig,
	collector: collector::Collector,
	queue: queue::Queue,
	worker: worker::Worker,
	proxer: proxer::Client,
}


impl ProxerWatcher {
	pub fn new() -> Self {

		let mdb = mongodb::Client::connect(DB_HOST, 27017)
			.unwrap()
			.db(DB_DBNAME);

		let mut client = proxer::Client::with_env_key("PROXER_API_KEY").unwrap();
		client.cooldown_time(time::Duration::new(3, 500_000_000));

		Self {
			config: WatcherConfig {},
			db: mdb,
			collector: collector::Collector::new(),
			queue: Queue::new(),
			worker: worker::Worker::new(),
			proxer: client,
		}
	}


	pub fn step_fwd(&mut self) {


		let job = match self.queue.next() {
			None => {
				info!("no jobs i queue found. Reindexing proxer...");
				self.reindex_proxer_list();
				info!("Indexing finished");
				return
			},
			Some(r) => r
		};



		info!("starting job");

		self.worker.process_job(job);


		debug!("removing job");
	}





	fn reindex_proxer_list(&self) -> i64 {

		let req = proxer::api::list::GetEntryList {
			is_h: None,
			kat: None,
			limit: Some(2500),
			medium: None,
			sort: Some("clicks".to_string()),
			sort_type: Some("DESC".to_string()),
			p: None,
			start: None
		};


		let mut client = self.proxer.clone();

		let pager = req.pager(client.clone());


		let mut new_found_counter = 0;

		for entry in pager {
			let entry = entry.unwrap();

			let mut hasher = Sha256::new();
			hasher.input_str(&entry.count.to_string());
			hasher.input_str(&entry.rate_sum.to_string());
			hasher.input_str(&entry.id.to_string());

			let hash = hasher.result_str();


			let coll = self.get_coll("InfoIndex");

			let f = coll.find_one(
				Some(doc!{
					"id": i64::from(entry.id.clone())
				}),
				None
			).unwrap();




			let waiter = QueuedObject {
				type_: QueuedObjectType::Info {id: entry.id},
				score: 0
			};


			if self.add_to_queue(waiter).is_some() {
				new_found_counter.add(1);
			}



			continue;


			match f {
				Some(_) => {
					warn!("{:7} entry indexed", entry.id);
				},
				None => {
					warn!("{:7} entry not indexed", entry.id);


					let info = client.execute(
						proxer::api::info::GetFullEntry::with_default(entry.id as usize)
					);

					println!("{:?}", info);

					coll.insert_one(doc!{
						"id": i64::from(entry.id.clone())
					}, None);
				}
			}
		}


		new_found_counter
	}

	fn get_coll(&self, coll_name: &str) -> mongodb::coll::Collection {
		self.db.collection(coll_name)
	}



	/// check if the entity is known
	fn is_in_index(&self) -> bool {
		// TODO: do the checking...

		false
	}


	fn add_to_queue(&self, waiter: QueuedObject) -> Option<()> {
		// TODO: Improve logic here...
		// filter for type and id
		// if no object was found, add it to the end
		// if an object was found, increade its score


		let coll = self.get_coll("Queue");

		let filter = QueueFilter {
			type_: waiter.type_.to_owned(),
		};


		info!("adding to queue: {}", filter);



		match coll.find_one(Some(filter.as_doc()), None).unwrap() {
			Some(_) => {
				info!("already there. Increase score");
				coll.update_one(
					filter.as_doc(),
					doc! {"$inc": {"score": 1}},
					None
				).unwrap();

				None
			},
			None => {
				info!("not there. Adding");
				coll.insert_one(waiter.as_doc(), None);

				Some(())
			}
		}
	}
}




#[derive(Debug, Clone, Serialize, Deserialize)]
struct QueueFilter {
	#[serde(rename = "type")]
	pub type_: QueuedObjectType,
}


impl fmt::Display for QueueFilter {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{:?}", self.type_)
	}
}


impl AsBsonDoc for QueueFilter {}





trait AsBsonDoc: serde::Serialize {
	fn as_doc(&self) -> bson::Document {
		bson::to_bson(self)
			.unwrap()
			.as_document()
			.unwrap()
			.clone()
	}
}



fn from_doc<T: DeserializeOwned>(doc: bson::Document) -> bson::DecoderResult<T> {
	bson::from_bson(bson::Bson::Document(doc))
}


fn to_doc<T: Serialize>(obj: &T) -> bson::EncoderResult<bson::Document> {
	match bson::to_bson(obj) {
		Ok(bson) => Ok(bson.as_document().unwrap().to_owned()),
		Err(e) => Err(e)
	}
}



pub enum CollectableData {
	FullEntry(pres::info::Fullentry),
	Comment(pres::info::Comment),
}
