extern crate chrono;
#[macro_use(Serialize, Deserialize)]
extern crate serde_derive;
extern crate serde_json;
extern crate serde;
extern crate postgres;
extern crate proxer;
#[macro_use]
extern crate log;
extern crate rand;
extern crate mongodb;
extern crate crypto;
extern crate pretty_env_logger;
#[macro_use(doc, bson)]
extern crate bson;
extern crate redis;
extern crate simple_redis;
extern crate bincode;

pub mod docs;
pub mod watcher;
pub mod psql;

use std::env;

pub const PG_DB: &str = "proxeranalytic";
pub const PG_USER: &str = "proxeranalytic";
pub const PG_PASSWD: &str = "proxeranalytic";
pub const PG_HOST: &str = "localhost";





fn main() {
	env::set_var("RUST_LOG", "ProxerAnalytic=debug,proxer=debug");
	pretty_env_logger::init().unwrap();



	psql::check_tables();


	std::process::exit(0);



	watcher::start();
}
