//! This module contains all the data models used by proxeranalytic

use chrono::NaiveDateTime;
use chrono::DateTime;
use chrono::offset::FixedOffset;
use chrono;
use proxer;
use proxer::response as p_res;
use std::fmt;
use bson::Document;
use bson;
use serde::Serialize;
use serde::de::DeserializeOwned;
use watcher::collector::Storable;

pub type Timestamp = DateTime<FixedOffset>;




pub trait ToDoc
where
	Self: Serialize
{
	fn to_doc(&self) -> Document {
		bson::to_bson(self.clone())
			.unwrap()
			.as_document()
			.unwrap()
			.to_owned()
	}
}


// pub trait FromDoc
// {
// 	fn to_bson(&self) -> Document {
// 		bson::to_bson(self.clone())
// 			.unwrap()
// 			.as_document()
// 			.unwrap()
// 	}
// }


#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum UpdateOps {
	#[serde(rename = "$currentDate")]
	CurrentDate,
	#[serde(rename = "$inc")]
	Inc,
	#[serde(rename = "$min")]
	Min,
	#[serde(rename = "$max")]
	Max,
	#[serde(rename = "$mul")]
	Mul,
	#[serde(rename = "$rename")]
	Rename,
	#[serde(rename = "$set")]
	Set,
	#[serde(rename = "$setOnInsert")]
	SetOnInsert,
	#[serde(rename = "$unset")]
	Unset,
}



trait Squeezable {
	fn squeeze(&self) -> Vec<Docs>;
}





#[derive(Debug, Clone)]
pub enum Docs {
	Comment(Comment),
	Name(Name),
	Info(Info),
	Tag(Tag),
	Genre(Genre),
	Rating(Rating),
	Progress(Progress),
	Userinfo(Userinfo),
}


impl fmt::Display for Docs {
	fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		let doc = match *self {
			Docs::Comment(_)  => "Comment",
			Docs::Name(_)     => "Name",
			Docs::Info(_)     => "Comment",
			Docs::Tag(_)      => "Tag",
			Docs::Genre(_)    => "Genre",
			Docs::Rating(_)   => "Rating",
			Docs::Progress(_) => "Progress",
			Docs::Userinfo(_) => "Userinfo",
		};

		write!(f, "{}", doc)
	}
}



#[derive(Hash, PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct Info {
	pub id: i64,
	pub count: i64,
	pub state: String,
	pub rate_sum: i64,
	pub rate_count: i64,
	pub clicks: i64,
}

impl ToDoc for Info {}


impl From<p_res::info::Fullentry> for Info {
	fn from(raw: p_res::info::Fullentry) -> Self {
		Self {
			id: raw.id,
			count: raw.count,
			state: raw.state,
			rate_sum: raw.rate_sum,
			rate_count: raw.rate_count,
			clicks: raw.clicks,
		}
	}
}


impl Storable for Info {
	const COLLECTION: &'static str = "Info";

	fn filter(&self) -> bson::Document {
		bson::to_bson(&self).unwrap().as_document().unwrap().to_owned()
	}
}





#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct Tag {
	/// Tag name
	pub name: String,
	/// link-id
	pub id: i64,
	/// Tag's id
	pub tag_id: i64,
	/// tagged at
	pub tagged_at: bson::UtcDateTime,
	/// is spoiler?
	pub spoiler: bool,
	/// is confirmed?
	pub matches: bool,
}


impl ToDoc for Tag {}




impl From<p_res::info::Tag> for Tag {
	fn from(tag: p_res::info::Tag) -> Self {
		Self {
			name: tag.tag,
			id: tag.id,
			tag_id: tag.tid,
			tagged_at: bson::UtcDateTime(tag.timestamp),
			spoiler: match tag.spoiler_flag {
				p_res::info::SpoilerFlag::Spoiler => true,
				p_res::info::SpoilerFlag::NoSpoiler => false,
			},
			matches: match tag.rate_flag {
				p_res::info::RateFlag::Match => true,
				p_res::info::RateFlag::NoMatch => false,
			},
		}
	}
}



#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct Genre {
	pub genre: String,
}


impl ToDoc for Genre {}



impl From<String> for Genre {
	fn from(genre: String) -> Self {
		Self {
			genre: genre
		}
	}
}




#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct Comment {
	info_id: i64,
	comment_id: i64,
	uid: i64,
	positive: i64,
	comment: String,
	time: bson::UtcDateTime,
}


impl ToDoc for Comment {}



impl From<p_res::info::Comment> for Comment {
	fn from(comment: p_res::info::Comment) -> Self {
		Self {
			info_id: comment.tid,
			comment_id: comment.id,
			uid: comment.uid,
			positive: comment.positive,
			comment: comment.comment,
			time: bson::UtcDateTime(DateTime::from_utc(comment.timestamp.naive_utc(), chrono::offset::Utc) ),
		}
	}
}





impl Into<Docs> for Comment {
	fn into(self) -> Docs {
		Docs::Comment(self)
	}
}








// impl PartialEq for Comment {
// 	fn eq(&self, other: &Comment) -> bool
// 	{
// 		if self.comment_id != other.comment_id {
// 			return false;
// 		};
// 		if self.info_id != other.info_id {
// 			return false;
// 		};
// 		if self.user_id != other.user_id {
// 			return false;
// 		};
// 		if self.positive != other.positive {
// 			return false;
// 		};
// 		if self.comment != other.comment {
// 			return false;
// 		};
//
// 		true
// 	}
// }



#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct Name {
	info_id: i64,
	name_id: i64,
	name: String,
	nametype: String,
}


impl ToDoc for Name {}


impl From<proxer::response::info::Name> for Name {
	fn from(name: proxer::response::info::Name) -> Self
	{
		Self {
			info_id: name.eid.into(),
			name_id: name.id.into(),
			name: name.name.into(),
			nametype: name.type_.into(),
		}
	}
}


impl Into<proxer::InfoID> for Name {
	fn into(self) -> proxer::InfoID
	{
		proxer::InfoID::from(self.info_id)
	}
}







#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct Rating {
	info_id: i64,
	uid: i64,
	rating: i64,
	positive: i64,
	meta: String,
}



impl ToDoc for Rating {}


impl From<p_res::info::Comment> for Rating {
	fn from(raw: p_res::info::Comment) -> Self
	{
		Self {
			info_id: raw.tid.into(),
			uid: raw.uid.into(),
			rating: raw.rating.into(),
			positive: raw.positive.into(),
			meta: raw.data,
		}
	}
}



impl Into<Docs> for Rating {
	fn into(self) -> Docs {
		Docs::Rating(self)
	}
}






#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
enum ProgressState {
	Cancelled,
	ToWatch,
	Watched,
	Watching,
}


impl ToDoc for ProgressState {}



impl From<p_res::info::WatchState> for ProgressState {
	fn from(state: p_res::info::WatchState) -> Self {
		match state {
			p_res::info::WatchState::Cancelled => ProgressState::Cancelled,
			p_res::info::WatchState::Watched => ProgressState::Watched,
			p_res::info::WatchState::Watching => ProgressState::Watching,
			p_res::info::WatchState::WillWatch => ProgressState::ToWatch,
		}
	}
}




#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct Progress {
	info_id: i64,
	uid: i64,
	episode: i64,
	state: ProgressState
}


impl ToDoc for Progress {}


impl From<p_res::info::Comment> for Progress {
	fn from(raw: p_res::info::Comment) -> Self
	{
		Self {
			info_id: raw.tid.into(),
			uid: raw.uid.into(),
			episode: raw.episode,
			state: ProgressState::from(raw.state)
		}
	}
}



impl Into<Docs> for Progress {
	fn into(self) -> Docs {
		Docs::Progress(self)
	}
}


impl Storable for Progress {
	const COLLECTION: &'static str = "Progress";

	fn filter(&self) -> bson::Document {
		bson::to_bson(&self).unwrap().as_document().unwrap().to_owned()
	}
}





#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct User {
	user_id: i64,
	username: String,
}




#[derive(PartialEq, Serialize, Deserialize, Debug, Clone)]
pub struct Userinfo {
	pub uid: i64,
	pub username: String,
	pub avatar: String,
	pub status: String,
	pub status_time: bson::UtcDateTime,
	pub points_uploads: i64,
	pub points_anime: i64,
	pub points_manga: i64,
	pub points_info: i64,
	pub points_forum: i64,
}


impl From<p_res::user::Userinfo> for Userinfo {
	fn from(user: p_res::user::Userinfo) -> Self {
		Self {
			uid: user.uid,
			username: user.username,
			avatar: user.avatar,
			status: user.status,
			status_time: bson::UtcDateTime(user.status_time),
			points_uploads: user.points_uploads,
			points_anime: user.points_anime,
			points_manga: user.points_manga,
			points_info: user.points_info,
			points_forum: user.points_forum,
		}
	}
}


impl Into<Docs> for Userinfo {
	fn into(self) -> Docs {
		Docs::Userinfo(self)
	}
}


impl ToDoc for Userinfo {}
