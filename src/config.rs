#[macro_use]
use serde_derive;
use toml;
use std::*;
use std::io::Read;


#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Config {
    pub proxer: ProxerConfig,
    pub database: DatabaseConfig
}


#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct ProxerConfig {
    pub apikey: String,
    pub cooldown: i64
}


#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DatabaseConfig {
    address: String,
    port: i64,
    collection: String
}


pub fn read() -> Option<Config> {
    let mut config_file = String::new();

    fs::File::open("pa.conf").expect("cant find 'pa.conf'")
        .read_to_string(&mut config_file)
        .expect("cant read config file");


        match toml::from_str(&config_file) {
            Ok(c) => Some(c),
            Err(e) => {
                println!("{}", e);
                return None;
            }
        }
}
